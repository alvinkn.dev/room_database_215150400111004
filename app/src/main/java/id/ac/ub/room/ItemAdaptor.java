package id.ac.ub.room;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class ItemAdaptor extends RecyclerView.Adapter<ItemAdaptor.ItemViewHolder> {
    LayoutInflater inflater;
    Context _context;
    ArrayList<Item> data;

    public ItemAdaptor(Context _context, ArrayList<Item> data) {
        this._context = _context;
        this.data = data;
        this.inflater = LayoutInflater.from(this._context);
    }
    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row, parent, false);
        return new ItemViewHolder(view);
    }
    public int getItemCount() {
        return data.size();
    }
    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        Item mhs = data.get(position);
        holder.tvNim.setText(mhs.getNim());
        holder.tvNama.setText(mhs.getNama());
    }

    class ItemViewHolder extends RecyclerView.ViewHolder{
        TextView tvNim;
        TextView tvNama;

        public ItemViewHolder(@NonNull View itemView) {
            super(itemView);
            tvNim = itemView.findViewById(R.id.tvNim);
            tvNama = itemView.findViewById(R.id.tvNama);
        }

    }

}
