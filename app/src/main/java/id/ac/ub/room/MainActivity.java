package id.ac.ub.room;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.Console;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    Button bt1;
    Button bt2;
    EditText etNama, etNim;
    RecyclerView rv1;
    ArrayList<Item> data;
    private AppDatabase appDb;
    int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        appDb = AppDatabase.getInstance(getApplicationContext());
        data = new ArrayList<>();
        ItemAdaptor itemAdaptor =new ItemAdaptor(this, data);
        bt1 = findViewById(R.id.bt1);
        bt2 = findViewById(R.id.bt2);
        rv1 = findViewById(R.id.rv1);
        rv1.setAdapter(itemAdaptor);
        rv1.setLayoutManager(new LinearLayoutManager(this));
        etNim = findViewById(R.id.etNim);
        etNama = findViewById(R.id.etNama);

        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Item item = new Item();
                item.setNama(etNama.getText().toString());
                item.setNim(etNim.getText().toString());
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        appDb.itemDao().insertAll(item);
                    }
                });

            }
        });
        bt2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppExecutors.getInstance().diskIO().execute(new Runnable() {
                    @Override
                    public void run() {
                        List<Item> list = appDb.itemDao().getAll();
                        Log.d("Data", list.toString());
                        data.clear();
                        data.addAll(list);
                    }
                });
                itemAdaptor.notifyDataSetChanged();
            }
        });
    }

}